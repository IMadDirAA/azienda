//
// Created by imadd on 08/04/2020.
//

#include "InteractionLine.h"


/**
 *
 * @param argc
 * @param argv
 * @return
 */
std::map<std::string, std::string> parser(unsigned int argc, char *argv[]) {
    /**
     * @var companyOptions which contain a map of all argument and options
     */
    std::map<std::string, std::string> companyOptions = {
            {"--id", ""},
            {"show", ""},
            {"--all", ""},
            {"--age", ""},
            {"add", ""},
            {"--work", ""},
            {"--help", ""},
            {"--limit", ""},
            {"remove", ""},
            {"passages", ""},
            {"--passage_type", ""},
            {"--version", ""},
            {"scheduler", ""},
            {"employees", ""},
            {"--last_name", ""},
            {"--first_name", ""}
    };

    // check if there
    if (argc == 0)
        return companyOptions;

    // transform char *argv[] to a vector<string>
    std::vector<std::string> args;
    args.reserve(argc);
    for (int i = 0; i < argc; ++i) {
        args.emplace_back(argv[i]);
    }

    if (args[0] == "--help") {
        companyOptions["--help"] = "true";
    } else if (args[0] == "employees") {
        companyOptions["employees"] = "true";
        // check if there isn't options or arguments
        if (argc == 1) {
            // employees without arguments
            throw std::invalid_argument("employees expected arguments");
        }

        std::vector<std::string> employeesOptions = {"--id", "--first_name", "--last_name", "--age", "--work"};
        unsigned int optionsCount = 1;
        if (args[optionsCount] == "add") {
            companyOptions["add"] = "true";
            optionsCount++;
            if (optionsCount == argc)
                return companyOptions;
            std::stringstream streamError;
            streamError << "unknown given arguments: ";
            for (unsigned int i = optionsCount; i < argc; ++i)
                streamError << args[i] << " ";
            throw std::invalid_argument(streamError.str());
        } else {
            if (args[optionsCount] == "show" or args[optionsCount] == "remove") {
                companyOptions[args[optionsCount]] = "true";
                optionsCount++;
                if (optionsCount < argc and args[optionsCount] == "--all") {
                    companyOptions[args[optionsCount]] = "true";
                    optionsCount++;
                    if (optionsCount < argc) {
                        std::stringstream streamError;
                        streamError << "unknown given arguments: ";
                        for (unsigned int i = optionsCount; i < argc; ++i)
                            streamError << args[i] << " ";
                        throw std::invalid_argument(streamError.str());
                    }
                    return companyOptions;
                }

                int oldOptionsCount = optionsCount;
                for (unsigned int i = optionsCount; i < argc; i += 2) {
                    for (auto &option: employeesOptions) {
                        if (args[i] == option) {
                            if (!companyOptions[option].empty()) {
                                // error id already taken
                                throw std::invalid_argument(option + " already taken");
                            }
                            if (i + 1 >= argc) {
                                // error expected argument
                                throw std::invalid_argument("expected " + option + " <argument>: no argument given");
                            }
                            if (isalpha(args[i + 1][0])) {
                                companyOptions[option] = args[i + 1];
                                optionsCount += 2;
                            } else {
                                // error invalid argument
                                throw std::invalid_argument("invalid " + option.substr(2) + " argument " + args[i + 1]);
                            }
                        }
                    }
                }
                if (oldOptionsCount == optionsCount) {
                    throw invalid_argument("expected at least one employee's field");
                }
            }
        }

        if (optionsCount < argc and args[optionsCount] == "--limit") {
            if (optionsCount + 1 >= argc) {
                // error expected argument
                throw std::invalid_argument("expected --limit <argument>: no argument given");
            }
            if (isdigit(args[optionsCount + 1][0])) {
                companyOptions["--limit"] = args[optionsCount + 1];
            } else {
                // error invalid argument
                throw std::invalid_argument("invalid --limit argument " + args[optionsCount + 1]);
            }
        } else if (optionsCount != argc) {
            std::stringstream streamError;
            streamError << "unknown given arguments: ";
            for (unsigned int i = optionsCount; i < argc; ++i)
                streamError << args[i] << " ";
            throw std::invalid_argument(streamError.str());
        }
    } else if (args[0] == "passages") {
        companyOptions["passages"] = "true";
        // check if there isn't options or arguments
        if (argc == 1) {
            // scheduler without arguments
            throw std::invalid_argument("passages expected arguments");
        }

        unsigned int optionsCount = 1;

        if (args[optionsCount] == "show") {
            companyOptions[args[optionsCount]] = "true";
            optionsCount++;
            if (optionsCount < argc and args[optionsCount] == "--all") {
                companyOptions[args[optionsCount]] = "true";
                optionsCount++;
            } else if (optionsCount < argc and args[optionsCount] == "--passage_type") {
                optionsCount++;
                if (optionsCount == argc) {
                    throw std::invalid_argument("expected --passage_type <passage_type> argument");
                } else if (args[optionsCount] != "ingress" and args[optionsCount] != "egress") {
                    throw invalid_argument("expected <passage_type> by ingress or egress instead given " + args[optionsCount]);
                } else {
                    companyOptions["--passage_type"] = args[optionsCount];
                    optionsCount++;
                }
            }
        } else if (args[optionsCount] == "add") {
            companyOptions[args[optionsCount]] = "true";
            optionsCount++;
            if (optionsCount == argc) {
                throw std::invalid_argument("expected <id> argument");
            } else if (isalpha(args[optionsCount][0])) {
                companyOptions["--id"] = args[optionsCount];
                optionsCount++;
            }
            if (optionsCount == argc) {
                throw std::invalid_argument("expected <passage_type> argument");
            } else if (args[optionsCount] != "ingress" and args[optionsCount] != "egress") {
                throw invalid_argument("expected <passage_type> by ingress or egress instead given " + args[optionsCount]);
            } else {
                companyOptions["--passage_type"] = args[optionsCount];
                optionsCount++;
            }
        }
        if (optionsCount < argc and args[optionsCount] == "--limit") {
            if (optionsCount + 1 >= argc) {
                // error expected argument
                throw std::invalid_argument("expected --limit <argument>: no argument given");
            }
            if (isdigit(args[optionsCount + 1][0])) {
                companyOptions["--limit"] = args[optionsCount + 1];
            } else {
                // error invalid argument
                throw std::invalid_argument("invalid --limit argument " + args[optionsCount + 1]);
            }
        } else if (optionsCount != argc) {
            std::stringstream streamError;
            streamError << "unknown given arguments: ";
            for (unsigned int i = optionsCount; i < argc; ++i)
                streamError << args[i] << " ";
            throw std::invalid_argument(streamError.str());
        }
    } else if (args[0] == "scheduler") {
        companyOptions["scheduler"] = "true";
        // check if there isn't options or arguments
        if (argc == 1) {
            // scheduler without arguments
            throw std::invalid_argument("scheduler expected arguments");
        }

        unsigned int optionsCount = 1;
        if (args[optionsCount] == "show") {
            companyOptions[args[optionsCount]] = "true";
            optionsCount++;
        } else {
            throw std::invalid_argument("expected <show> argument");
        }

        if (argc == optionsCount) {
            throw std::invalid_argument("expected <id> argument");
        } else if (isalpha(args[optionsCount][0])) {
            companyOptions["--id"] = args[optionsCount];
            optionsCount++;
        } else {
            throw std::invalid_argument("invalid " + args[optionsCount] + " arguments passed");
        }
        if (optionsCount != argc) {
            std::stringstream streamError;
            streamError << "unknown given arguments: ";
            for (unsigned int i = optionsCount; i < argc; ++i)
                streamError << args[i] << " ";
            throw std::invalid_argument(streamError.str());
        }
    } else {
        // unknown argument
        throw std::invalid_argument("invalid arguments syntax pattern " + args[0]);
    }
    return companyOptions;
}

Employee *getEmployeeFromConsole() {
    std::string firstName, lastName, work;
    unsigned int age;
    std::cout << "------------------insert employee data------------------" << std::endl;
    std::cout << "\tFirst Name: ";
    std::cin >> firstName;
    std::cout << "\tLast Name: ";
    std::cin >> lastName;
    std::cout << "\tWork: ";
    std::cin >> work;
    do {
        std::cout << "\tAge: ";
        std::cin >> age;
    } while (age < 1);
    return new Employee((int)age, firstName, lastName, work);
}

void showEmployees(std::vector<Employee> employees) {
    TextTable t( '-', '|', '+' );
    t.add("    ");
    t.add("ID    ");
    t.add("First Name    ");
    t.add("Last Name    ");
    t.add("Age    ");
    t.add("Work     ");
    t.add("enable     ");
    t.endOfRow();
    int count = 1;
    for (auto &employee: employees) {
        t.add(std::to_string(count++));
        t.add(employee.getID());
        t.add(employee.getFirstName());
        t.add(employee.getLastName());
        t.add(std::to_string(employee.getAge()));
        t.add(employee.getWork());
        if (employee.isEnable()) {
            t.add("True");
        } else {
            t.add("False");
        }
        t.endOfRow();
    }
    std::cout << t;
}

void showScheduler(std::vector<ChronoGraph> chronoGraphs) {
    TextTable t( '-', '|', '+' );
    t.add("             ");
    t.add("ID               ");
    t.add("Employee Name    ");
    t.add("ingress at       ");
    t.add("egress at        ");
    t.add("working time(S)  ");
    t.endOfRow();
    int count = 1;
    for (auto &chronoGraph: chronoGraphs) {
        t.add(std::to_string(count++));
        t.add(chronoGraph.getEmployee().getID());
        t.add(chronoGraph.getEmployee().getFirstName() + " " + chronoGraph.getEmployee().getLastName());
        if (chronoGraph.getIngressPassage()) {
            tm *ltm = chronoGraph.getIngressLtm();
            t.add(std::to_string(ltm->tm_mday) + "/" + std::to_string(ltm->tm_mon + 1) + "/" + std::to_string(ltm->tm_year + 1900) + "  " + std::to_string(ltm->tm_hour) + ":" + std::to_string(ltm->tm_min));
        } else {
            t.add("Undefined");
        }
        if (chronoGraph.getEgressPassage()) {
            tm *ltm = chronoGraph.getEgressLtm();
            t.add(std::to_string(ltm->tm_mday) + "/" + std::to_string(ltm->tm_mon + 1) + "/" + std::to_string(ltm->tm_year + 1900) + "  " + std::to_string(ltm->tm_hour) + ":" + std::to_string(ltm->tm_min));
        } else {
            t.add("Undefined");
        }
        if (chronoGraph.getIngressPassage() and chronoGraph.getEgressPassage()) {
            t.add(std::to_string((int)(chronoGraph.getDiffTime())));
        } else {
            t.add("Undefined");
        }
        t.endOfRow();
    }
    std::cout << t;
}

void showPassages(std::vector<Passage> passages) {
    TextTable t( '-', '|', '+' );
    t.add("    ");
    t.add("ID    ");
    t.add("Type    ");
    t.add("Date    ");
    t.endOfRow();
    int count = 1;
    for (auto &passage: passages) {
        t.add(std::to_string(count++));
        t.add(passage.getId());
        if (passage.getPassageType() == PassageType::ingress) {
            t.add("ingress");
        } else {
            t.add("egress");
        }
        tm *ltm = passage.getLtm();
        t.add(std::to_string(ltm->tm_mday) + "/" + std::to_string(ltm->tm_mon + 1) + "/" + std::to_string(ltm->tm_year + 1900) + "  " + std::to_string(ltm->tm_hour) + ":" + std::to_string(ltm->tm_min));
        t.endOfRow();
    }
    std::cout << t;
}
