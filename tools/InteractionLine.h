//
// Created by imadd on 08/04/2020.
//

#ifndef COMPANY_INTERACTIONLINE_H
#define COMPANY_INTERACTIONLINE_H
#include <vector>
#include <map>
#include "../chronoGraph/ChronoGraph.h"
#include "TextTable.h"


std::map<std::string, std::string> parser(unsigned int argc, char *argv[]);
Employee *getEmployeeFromConsole();
void showEmployees(std::vector<Employee> employees);
void showScheduler(std::vector<ChronoGraph> chronoGraphs);
void showPassages(std::vector<Passage> passages);


#endif //COMPANY_INTERACTIONLINE_H
