#include <iostream>
#include <sstream>
#include <vector>
#include <string>
#include <map>
#include "chronoGraph/ChronoGraphEmployees.h"
#include "tools/InteractionLine.h"

/**
 * @var usage which contain usage commands
 */
static const std::string usage = R"(
Company.

Usage:
    scheduler show <id>
    employees {show | remove} {[--all] | [--id <id>] [--first_name <first_name>] [--last_name <last_name>] [--work <work>] [--age <age>] [--limit <limit>]}
    employees add
    passages show {--all | [--passage_type <passage_type>]} [--limit <limit>]
    passages add <id> <passage_type>


Options:
    --help                         Show this screen.
    --all                          include all employees or passages
    --id <id>                      employee or passage id
    --first_name <first_name>      employee first_name
    --passage_type <passage_type>  passage type
    --last_name <last_name>        employee last_name
    --work <work>                  employee work
    --age <age>                    employee age
    --limit <limit>                records limit

)";

int main(int argc, char *argv[]) {
    // check if there is arguments
    if (argc == 1) {
        // show usage
        std::cout << usage;
        exit(0);
    }

    std::map<std::string, std::string> arguments;
    // check arguments pattern
    try {
        arguments = parser(argc - 1, argv + 1);
    } catch (const std::invalid_argument &err) {
        std::cerr << "error: " << err.what() << std::endl;
        std::cout << usage;
        exit(-1);
    }

    if (arguments["--help"] == "true") {
        std::cout << usage;
        exit(-1);
    }

    // executing the request
    // company components
    auto employeesComponent = new EmployeesComponent();
    auto passagesComponent = new PassagesComponent();
    auto chronoGraphComponent = new ChronoGraphEmployees();

    // scheduler action
    if (arguments["scheduler"] == "true") {
        // read all employee equal to the argument id
        auto employees = employeesComponent->read([&arguments](Employee employee) {
            return employee.getID() == arguments["--id"];
        });
        // if there is at least one employee
        if (employees.size()) {
            showScheduler(chronoGraphComponent->getChronoGraphs(employees[0]));
        } else {
            std::cout << "no data found";
            exit(0);
        }
    } else if (arguments["passages"] == "true") {     // passages action
        if (arguments["add"] == "true") { // add action
            PassageType passageType = PassageType::egress;
            if (arguments["--passage_type"] == "ingress") {
                passageType = PassageType::ingress;
            }
            auto passage = new Passage(time(0), passageType, arguments["--id"]);
            try {
                passagesComponent->add(*passage);
            } catch (std::invalid_argument &err) {
                std::cerr << "error: " << err.what();
                exit(-1);
            }
            std:: cout << "passage added successfully";
        } else if(arguments["show"] == "true") { // show action
            // get all passages with the given arguments
            auto passages = passagesComponent->read([&arguments](Passage passage) {
                static int count = std::atoi(arguments["--limit"].c_str());
                PassageType passageType;
                if (arguments["--passage_type"] == "ingress") {
                    passageType = PassageType::ingress;
                } else {
                    passageType = PassageType::egress;
                }
                return
                        (arguments["--all"] == "true") or
                        ((!arguments["--passage_type"].empty() and passage.getPassageType() == passageType)) and
                        (arguments["--limit"].empty() or count--);
            });
            // show passages
            showPassages(passages);
            exit(0);
        }
    } else if (arguments["employees"] == "true") {    // employees action
        if (arguments["add"] == "true") { // add action
            auto employee = getEmployeeFromConsole();
            employeesComponent->add(*employee);
            std::cout << "employee added successfully" << std::endl;
            exit(0);
        } else {
            std::function<bool(Employee)> employeesQuery = [&arguments](Employee employee) {
                static int count = std::atoi(arguments["--limit"].c_str());
                return
                        (arguments["--all"] == "true") or
                        ((!arguments["--id"].empty() and employee.getID() == arguments["--id"]) or
                         (!arguments["--first_name"].empty() and employee.getFirstName() == arguments["--first_name"]) or
                         (!arguments["--last_name"].empty() and employee.getLastName() == arguments["--last_name"]) or
                         (!arguments["--work"].empty() and employee.getWork() == arguments["--work"]) or
                         (!arguments["--age"].empty() and employee.getAge() == std::stoi(arguments["--age"]))) and
                        (arguments["--limit"].empty() or count--);
            };
            if (arguments["show"] == "true") { // show action
                auto employees = employeesComponent->read(employeesQuery);
                showEmployees(employees);
            } else if (arguments["remove"] == "true") { // remove action
                auto removedmployees = employeesComponent->remove(employeesQuery);
                std::cout << "\nremoved " << removedmployees << " employees";
            }
        }
    }
    return 0;
}
