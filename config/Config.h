//
// Created by imadd on 28/03/2020.
//

#ifndef AZIENDA_CONFIG_H
#define AZIENDA_CONFIG_H

#include <yaml-cpp/yaml.h>
#include <string>
#include <iostream>
#include <fstream>

/**
 * @class Config used to get azienda settings
 */
class Config {
public:
    /**
     * default constructor which initialize the configData from the file
     * and create the file with default setting if not exist
     */
    Config();

    /**
     * remove the componentName setting
     * @param componentName which contains name of component
     * @param key which contains key of componentName settings
     * @return true if the component and key exist otherwise return false
     */
    bool remove(const std::string &componentName, const std::string &key);

    /**
     * get the componentName setting
     * @param componentName which contains name of component
     * @param key which contains key of componentName settings
     * @return string value of key, if the key and the componentName not exist return ""
     */
    std::string get(const std::string &componentName, const std::string &key);

    /**
     * set the component settings
     * @param componentName which contains name of component
     * @param key which contains key of componentName settings
     * @param value contains the value to be assigned to the keu
     * @return true if the component exist
     */
    bool set(const std::string &componentName, const std::string &key, const std::string &value);

    /**
     * check if the key exist
     * @param componentName which contains name of component
     * @param key which contains key of componentName settings
     * @return true if the key exist
     */
    bool isKeyExist(const std::string &componentName, const std::string &key);

    // config file name
    static std::string configFileName;
private:
    // config data var
    static YAML::Node* configData;

    /**
     * check if the file exist
     * @param fileName
     * @return true if the file exist otherwise false
     */
    bool isFileExist(const std::string &fileName);

    /**
     * create config file with the default setting
     * the function will be called when the file not exist
     */
    void createDefaultConfig();

    /**
     * save the configuration when data is set
     */
    void saveConfig();
};


#endif //AZIENDA_CONFIG_H
