//
// Created by imadd on 28/03/2020.
//

#include "Config.h"


Config::Config() {
    if (Config::configFileName.empty()) {
        Config::configData = new YAML::Node();
        Config::configFileName = "../Config.yaml";
    }
    if (!isFileExist(Config::configFileName))
        this->createDefaultConfig();
    if ((*Config::configData).IsNull())
        *Config::configData = YAML::LoadFile(Config::configFileName);
}

std::string Config::get(const std::string &componentName, const std::string &key) {
    try {
        return (*Config::configData)[componentName][key].as<std::string>();
    } catch (const YAML::Exception &err) {
        return "";
    }
}

bool Config::set(const std::string &componentName, const std::string &key, const std::string &value) {
    try {
        (*Config::configData)[componentName][key] = value;
    } catch (const YAML::Exception &err) {
        return false;
    }
    this->saveConfig();
    return true;
}

bool Config::isFileExist(const std::string &fileName) {
    FILE *fp = fopen(fileName.c_str(), "r");
    if (fp){
        fclose(fp);
        return true;
    }
    return errno != ENOENT;
}

bool Config::isKeyExist(const std::string &componentName, const std::string &key) {
    return (*Config::configData)[componentName].IsDefined() and (*Config::configData)[componentName][key].IsDefined();
}

void Config::createDefaultConfig() {
    YAML::Emitter out;

    out << YAML::Comment("this file is used to configure azienda settings");
    out << YAML::Newline;
    out << YAML::Comment("created by Imad Diraa <imad.diraa1092@gmail.com>");
    out << YAML::Newline << YAML::Newline << YAML::Newline;


    out << YAML::BeginMap;

    out << YAML::Comment("employees settings");
    out << YAML::Key << "employees";
    out << YAML::BeginMap;
    out << YAML::Key << "maxLength";
    out << YAML::Value << 999;
    out << YAML::Key << "erase_data";
    out << YAML::Value << false;
    out << YAML::Key << "file";
    out << YAML::Value << "employees.txt";
    out << YAML::Comment("supported extensions {TXT}");
    out << YAML::EndMap;

    out << YAML::Newline << YAML::Newline << YAML::Newline;

    out << YAML::Comment("passages settings");
    out << YAML::Key << "passages";
    out << YAML::BeginMap;
    out << YAML::Key << "file";
    out << YAML::Value << "passages.txt";
    out << YAML::Comment("supported extensions {TXT}");
    out << YAML::EndMap;

    out << YAML::Newline << YAML::Newline << YAML::Newline;

    out << YAML::Comment("orario settings");
    out << YAML::Key << "orario";
    out << YAML::Value << YAML::Node(YAML::NodeType::Null);
    out << YAML::EndMap;
    out << YAML::Newline << YAML::Newline << YAML::Newline;

    std::ofstream fout(Config::configFileName, std::ios::ate);
    if (fout.fail()) {
        throw std::ios::failure("fail opening file");
    }
    fout << out.c_str();
    fout.close();
}

void Config::saveConfig() {
    YAML::iterator iterator;
    YAML::Emitter out;
    out << YAML::Comment("this file is used to configure azienda settings");
    out << YAML::Newline;
    out << YAML::Comment("created by Imad Diraa <imad.diraa1092@gmail.com>");
    out << YAML::Newline << YAML::Newline << YAML::Newline;


    out << YAML::BeginMap;

    out << YAML::Comment("employees settings");
    out << YAML::Key << "employees";
    if (!(*Config::configData)["employees"].size())
        out << YAML::Value << YAML::Node(YAML::NodeType::Null);
    else
        out << (*Config::configData)["employees"];

    out << YAML::Newline << YAML::Newline << YAML::Newline;

    out << YAML::Comment("passages settings");
    out << YAML::Key << "passages";
    if (!(*Config::configData)["passages"].size())
        out << YAML::Value << YAML::Node(YAML::NodeType::Null);
    else
        out << (*Config::configData)["passages"];


    out << YAML::Newline << YAML::Newline << YAML::Newline;

    out << YAML::Comment("orario settings");
    out << YAML::Key << "orario";
    if (!(*Config::configData)["orario"].size())
        out << YAML::Value << YAML::Node(YAML::NodeType::Null);
    else
        out << (*Config::configData)["orario"];

    out << YAML::EndMap;
    out << YAML::Newline << YAML::Newline << YAML::Newline;

    std::ofstream fout(Config::configFileName, std::ios::ate);

    fout << out.c_str();
    fout.close();
}

bool Config::remove(const std::string &componentName, const std::string &key) {
    if (this->isKeyExist(componentName, key)) {
        (*Config::configData)[componentName].remove(key);
        this->saveConfig();
        return true;
    }
    return false;
}

std::string Config::configFileName;
YAML::Node* Config::configData;
