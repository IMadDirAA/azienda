//
// Created by imadd on 27/03/2020.
//

#ifndef AZIENDA_EMPLOYEESCONTAINER_H
#define AZIENDA_EMPLOYEESCONTAINER_H

#include <bits/ios_base.h>
#include <vector>
#include <list>
#include <stdexcept>
#include <ctime>
#include <chrono>
#include <cstdlib>
#include <functional>
#include "Employee.h"
#include "../config/Config.h"

class EmployeesComponent {
private:
    static string fileName;
    static Config *config;
    static list<Employee> container;
    void save();
public:

    /**
     * EmployeesComponent()
     */
    explicit EmployeesComponent();

    /**
     * add() add employee
     * @param employee
     */
    void add(Employee &employee);

    /**
     * read() search an employee with the id passed like parameter and return a copy
     * @tparam prediction is a function which select Employees
     * @return vector of employees selected by prediction function
     */
    vector<Employee> read(function<bool(Employee)> prediction);

    /**
     * remove() disable an employee with Id passed like parameter
     * @tparam prediction is a function which select Employees
     * @return true number of removed employees
     */
    unsigned int remove(function<bool(Employee)> prediction);

    /**
     * generateUniqueId()
     * @return
     */
    static string generateUniqueId();


};


#endif //AZIENDA_EMPLOYEESCONTAINER_H
