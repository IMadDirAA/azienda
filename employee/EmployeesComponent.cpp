//
// Created by imadd on 27/03/2020.
//

#include "EmployeesComponent.h"

void EmployeesComponent::add(Employee &employee) {
    if (employee.getWork() == Employee::emptyString)
        throw invalid_argument("ContenitoreEmployee: employee passed without work");
    if (employee.getFirstName() == Employee::emptyString && employee.getLastName() == Employee::emptyString)
        throw invalid_argument("ContenitoreEmployee: employee passed without name");

    employee.setID(EmployeesComponent::generateUniqueId());
    Employee* newEmployee = new Employee();
    *newEmployee = employee;
    EmployeesComponent::container.push_back(*newEmployee);
    this->save();
}

vector<Employee> EmployeesComponent::read(function<bool(Employee)> prediction) {
    vector<Employee> employees;
    list<Employee>::iterator employee;
    for (employee = EmployeesComponent::container.begin(); employee != EmployeesComponent::container.end() ; employee++)
        if (prediction(*employee))
                employees.push_back(*employee);
    return employees;
}

unsigned int EmployeesComponent::remove(function<bool(Employee)> prediction) {
    unsigned int removedEmployeesCount = 0;
    list<Employee>::iterator employee;
    for (employee = EmployeesComponent::container.begin(); employee != EmployeesComponent::container.end(); employee++) {
        if (prediction(*employee)) {
            if (EmployeesComponent::config->get("employees", "erase_data") == "true") {
                employee = EmployeesComponent::container.erase(employee);
                removedEmployeesCount++;
            } else if (employee->isEnable()) {
                employee->setEnable(false);
                removedEmployeesCount++;
            }
        }
    }
    if (removedEmployeesCount)
        this->save();
    return removedEmployeesCount;
}

string EmployeesComponent::generateUniqueId() {
        std::chrono::milliseconds mil(0);
        char hexString[20];
        itoa(time(0) * 1000 + mil.count(), hexString, 36);
        return string(hexString);
}

void EmployeesComponent::save() {
    FILE* fp = fopen(fileName.c_str(), "w");
    list<Employee>::iterator employee;
    for (employee = EmployeesComponent::container.begin(); employee != EmployeesComponent::container.end(); employee++) {
        fprintf(
                fp,
                "\n%d %d %s %s %s %s",
                employee->isEnable(),
                employee->getAge(),
                employee->getFirstName().c_str(),
                employee->getLastName().c_str(),
                employee->getWork().c_str(),
                employee->getID().c_str()
                );
    }
    fclose(fp);
}

EmployeesComponent::EmployeesComponent() {
    if (EmployeesComponent::fileName.empty()) {
        EmployeesComponent::config = new Config();
        EmployeesComponent::fileName = EmployeesComponent::config->get("employees", "file");
        // check if the file exist
        FILE* fp = fopen(fileName.c_str(), "r");
        if (!fp) {
            // check if the can be created
            fp = fopen(fileName.c_str(), "w");
            if (!fp)
                throw ios_base::failure("can't create file : " + fileName);
            fclose(fp);
            return;
        }

        // temp vars to get data from file and passed it to object
        char firstName[100], lastName[100], work[100], ID[100];
        int age;
        bool enable;
        Employee* dip;

        while (!feof(fp)) {
            if (fscanf(fp, "%d %d %s %s %s %s", &enable, &age, firstName, lastName, work, ID) > 0) {
                dip = new Employee(age, string(firstName), string(lastName), string(work), string(ID), enable);
                container.push_back(*dip);
            }
        }
        fclose(fp);
    }
}

Config *EmployeesComponent::config = nullptr;
list<Employee> EmployeesComponent::container;
string EmployeesComponent::fileName;
