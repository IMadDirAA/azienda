//
// Created by imadd on 27/03/2020.
//

#include "Employee.h"

Employee::Employee():Person() {
    this->enable = true;
}

Employee::Employee(int age, string firstName, string lastName, string work, string ID, bool enable)
: Person(age, firstName, lastName) {
    this->setWork(work);
    this->setID(ID);
    this->enable = enable;
}

void Employee::setID(string ID) {
    if (ID.empty())
        this->ID = Employee::emptyString;
    else
        this->ID = ID;
}

const string Employee::getID() const {
    return this->ID;
}

void Employee::setWork(string work) {
    if (work.empty())
        this->work = Employee::emptyString;
    else
        this->work = work;
}

const string Employee::getWork() const {
    return this->work;
}

void Employee::setEnable(bool enable) {
    this->enable = enable;
}

Employee &Employee::operator=(const Employee &employee) {
    this->ID = employee.ID;
    this->firstName = employee.firstName;
    this->lastName = employee.lastName;
    this->work = employee.work;
    this->age = employee.age;
    this->enable = employee.enable;
    return *this;
}

const string Employee::emptyString = "#";

const bool Employee::isEnable() {
    return this->enable;
}

bool Employee::operator==(const Employee &employee) {
    return this->ID == employee.ID;
}
