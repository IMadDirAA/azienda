//
// Created by imadd on 27/03/2020.
//

#ifndef AZIENDA_PERSON_H
#define AZIENDA_PERSON_H

#include <string>

using namespace std;

class Person{

protected:
    string firstName;
    string lastName;
    int age;
public:
    /**
     * default constructor which initialize the object
     */
    Person();

    /**
     * constructor person which initialize the object with the parameters passed
     * @param age
     * @param firstName
     * @param lastName
     */
    Person(int age, string firstName, string lastName);

    /**
     * setFirstName
     * @param firstName
     */
    void setFirstName(string firstName);

    /**
     * getFirstName
     * @return firstName
     */
    const string getFirstName() const;

    /**
     * setLastName()
     * @param lastName
     */
    void setLastName(string lastName);

    /**
     * getLastName()
     * @return lastName
     */
    const string getLastName() const;

    /**
     * setAge()
     * @param age
     */
    void setAge(int age);

    /**
     * getAge()
     * @return age
     */
    const int getAge() const;
};

#endif //AZIENDA_PERSON_H
