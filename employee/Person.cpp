//
// Created by imadd on 27/03/2020.
//

#include "Person.h"

Person::Person() {
    this->age = 0;
    this->firstName = "#";
    this->lastName = "#";
}

Person::Person(int age, string firstName, string lastName) {
    this->age = age;
    this->setFirstName(firstName);
    this->setLastName(lastName);
}

void Person::setFirstName(string firstName) {
    if (firstName.empty())
        this->firstName = "#";
    else
        this->firstName = firstName;
}

const string Person::getFirstName() const {
    return this->firstName;
}

void Person::setLastName(string lastName) {
    if (lastName.empty())
        this->lastName = "#";
    else
        this->lastName = lastName;
}

const string Person::getLastName() const {
    return this->lastName;
}

void Person::setAge(int age) {
    this->age = age;
}

const int Person::getAge() const {
    return this->age;
}
