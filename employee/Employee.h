//
// Created by imadd on 27/03/2020.
//

#ifndef AZIENDA_EMPLOYEE_H
#define AZIENDA_EMPLOYEE_H

#include "Person.h"

class Employee: public Person {
private:
    string ID;
    string work;
    bool enable;
public:
    static const string emptyString;

    /**
     * default constructor which initialize the object propriety
     */
    Employee();

    /**
     * constructor employee which initialize the object propriety with passed parameters
     * @param age
     * @param firstName
     * @param lastName
     * @param work (default="#")
     * @param ID (default="#")
     * @param enable (default="true")
     */
    Employee(int age, string firstName, string lastName, string work = "#", string ID = "#", bool enable = true);

    /**
     * setID()
     * @param ID
     */
    void setID(string ID);

    /**
     * getId()
     * @return
     */
    const string getID() const;

    /**
     * setWork()
     * @param work
     */
    void setWork(string work);

    /**
     * getWork()
     * @return
     */
    const string getWork() const;

    /**
     * setEnable
     * @param enable (default=true)
     */
    void setEnable(bool enable = true);

    /**
     * iisEnable()
     * @return true if is enabled otherwise return false
     */
    const bool isEnable();

    /**
     * operator = which copy all data from this object to the object passed
     * @param employee
     * @return
     */
    Employee& operator= (const Employee &employee);

    /**
     * operator = which copy all data from this object to the object passed
     * @param employee
     * @return
     */
    bool operator== (const Employee &employee);
};


#endif //AZIENDA_EMPLOYEE_H
