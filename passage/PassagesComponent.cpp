//
// Created by imadd on 30/03/2020.
//

#include "PassagesComponent.h"
#include <functional>

PassagesComponent::PassagesComponent() {
    if (PassagesComponent::fileName.empty()) {
        PassagesComponent::config = new Config();
        PassagesComponent::employeesComponent = new EmployeesComponent();
        PassagesComponent::fileName = config->get("passages", "file");
        // check if the file exist
        FILE* fp = fopen(fileName.c_str(), "r");
        if (!fp) {
            // check if the can be created
            fp = fopen(fileName.c_str(), "w");
            if (!fp)
                throw std::ios_base::failure("can't create file : " + fileName);
            fclose(fp);
            return;
        }

        // temp vars to get data from file and passed it to object
        time_t time;
        char ID[100];
        enum PassageType passageType;
        Passage* passage;

        while (!feof(fp)) {
            if (fscanf(fp, "%d %d %s", &passageType, &time, ID) > 0) {
                passage = new Passage(time, passageType, std::string(ID));
                PassagesComponent::container.push_back(*passage);
            }
        }
        fclose(fp);
    }
}

void PassagesComponent::save() {
    FILE* fp = fopen(fileName.c_str(), "w");
    std::vector<Passage>::iterator passage;
    for (passage = PassagesComponent::container.begin(); passage != PassagesComponent::container.end(); passage++) {
        fprintf(
                fp,
                "\n%d %d %s",
                passage->getPassageType(),
                passage->getTime(),
                passage->getId().c_str()
        );
    }
    fclose(fp);
}

std::vector<Passage> PassagesComponent::read(function<bool(Passage)> f) {
    Passage *passage = nullptr;
    std::vector<Passage> passages;
    auto passageIteratore = PassagesComponent::container.begin();
    for (; passageIteratore != PassagesComponent::container.end() ; passageIteratore++)
        if (f(*passageIteratore)) {
            passage = new Passage(passageIteratore->getTime(), passageIteratore->getPassageType(), passageIteratore->getId());
            passages.push_back(*passage);
        }
    return passages;
}

void PassagesComponent::add(const Passage &passage) {
    std::vector<Employee> employees = employeesComponent->read([passage](Employee employee){
        return passage.getId() == employee.getID();
    });
    if (employees.empty())
        throw std::invalid_argument("passage id must coincide with a employee");
    if (passage.getPassageType() != PassageType::ingress && passage.getPassageType() != PassageType::egress)
        throw std::invalid_argument("invalid passage type");
    if (passage.getTime() < 0)
        throw std::invalid_argument("invalid passage time");
    PassagesComponent::container.push_back(passage);
    this->save();
}

Config *PassagesComponent::config = nullptr;
string PassagesComponent::fileName;
EmployeesComponent *PassagesComponent::employeesComponent = nullptr;
vector<Passage> PassagesComponent::container;