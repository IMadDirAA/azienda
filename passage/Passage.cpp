//
// Created by imadd on 30/03/2020.
//

#include "Passage.h"

time_t Passage::getTime() const {
    return this->timeInSeconds;
}

void Passage::setTime(time_t time) {
    this->timeInSeconds = time;
}

PassageType Passage::getPassageType() const {
    return this->passageType;
}

void Passage::setPassageType(PassageType passageType) {
    this->passageType = passageType;
}

const std::string &Passage::getId() const {
    return this->ID;
}

void Passage::setId(const std::string &id) {
    this->ID = id;
}

Passage::Passage(time_t time, PassageType passageType, std::string ID) {
    this->passageType = passageType;
    this->timeInSeconds = time;
    this->ID = ID;
}

Passage::Passage() {
    this->passageType = PassageType::ingress;
    this->timeInSeconds = time(0);
}

Passage &Passage::operator=(const Passage &passage) {
    this->ID = passage.ID;
    this->timeInSeconds = passage.timeInSeconds;
    this->passageType = passage.passageType;
    return *this;
}

tm *Passage::getLtm() const {
    return localtime(&this->timeInSeconds);
}
