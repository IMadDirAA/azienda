//
// Created by imadd on 30/03/2020.
//

#ifndef AZIENDA_PASSAGESCONTAINER_H
#define AZIENDA_PASSAGESCONTAINER_H

#include <vector>
#include <functional>
#include <bits/ios_base.h>
#include "Passage.h"
#include "../employee/EmployeesComponent.h"
#include "../config/Config.h"

class PassagesComponent {
private:
    static Config *config;
    static std::string fileName;
    static EmployeesComponent* employeesComponent;
    static std::vector<Passage> container;
    void save();
public:
    /**
     *
     * @param fileName
     * @throw ios_base::failure exception is thrown when the file doesn't exist and can't be created
     * @param employeesComponent
     */
    explicit PassagesComponent();

    /**
     * add() add passage
     * @throw invalid_argument exception is thrown in the following three cases:
     *                              -1) the passages ID not correspond at any employee
     *                              -2) the passage type value is unknown
     *                              -3) the passage time value is negative
     * @param passage
     */
    void add(const Passage &passage);

    /**
     *
     * @param f the function which select the wanted passages
     * @return
     */
    std::vector<Passage> read(function<bool(Passage)> f);
};


#endif //AZIENDA_PASSAGESCONTAINER_H
