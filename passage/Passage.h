//
// Created by imadd on 30/03/2020.
//

#ifndef AZIENDA_PASSAGE_H
#define AZIENDA_PASSAGE_H

#include <ctime>
#include <string>

// passage type
enum PassageType {ingress, egress};

/**
 * @class Passage which represent the employee passage
 * @author Diraa Imad <imad.diraa1092@gmail.com>
 */
class Passage {
private:
    /**
     * @var timeInSeconds propriety which used to contain the passage time in seconds
     */
    time_t timeInSeconds;

    /**
     * @var passageType propriety which contain the passage type
     * @example {egress, entrance}
     */
    PassageType passageType;

    /**
     * @var ID which represent the employee that make this passage
     */
    std::string ID;

public:
    /**
     * default constructor which initialize the object proprieties
     */
    Passage();

    /**
     * constructor which initialize the object with the passed parameters
     * @param time
     * @param passageType
     * @param ID
     */
    Passage(time_t time, PassageType passageType, std::string ID);

    /**
     * getTime()
     * @return the time in seconds of passage
     */
    time_t getTime() const;

    /**
     * getLtm()
     * @return the local time structure initialized by timeInSeconds propriety
     */
    tm *getLtm() const;

    /**
     * set the time of passage
     * @param time which contains the time in seconds
     */
    void setTime(time_t time);

    /**
     * getPassageType
     * @return the passage type {egress, entrance}
     */
    PassageType getPassageType() const;

    /**
     * set the passage type
     * @param passageType contains the passage type
     */
    void setPassageType(PassageType passageType);

    /**
     * get the id
     * @return id which represent a employee id
     */
    const std::string &getId() const;

    /**
     * the function set the id
     * @param id which represent a employee id
     */
    void setId(const std::string &id);

    /**
     * overload operator = used to copy all data from object to another
     * @param passage the object which contain the data
     * @return the reference of object which updated with new data
     */
    Passage& operator= (const Passage &passage);
};


#endif //AZIENDA_PASSAGE_H
