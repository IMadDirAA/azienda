# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "C:/yaml-cpp/test/integration/emitter_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/emitter_test.cpp.obj"
  "C:/yaml-cpp/test/integration/encoding_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/encoding_test.cpp.obj"
  "C:/yaml-cpp/test/integration/error_messages_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/error_messages_test.cpp.obj"
  "C:/yaml-cpp/test/integration/gen_emitter_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/gen_emitter_test.cpp.obj"
  "C:/yaml-cpp/test/integration/handler_spec_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/handler_spec_test.cpp.obj"
  "C:/yaml-cpp/test/integration/handler_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/handler_test.cpp.obj"
  "C:/yaml-cpp/test/integration/load_node_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/load_node_test.cpp.obj"
  "C:/yaml-cpp/test/integration/node_spec_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/integration/node_spec_test.cpp.obj"
  "C:/yaml-cpp/test/main.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/main.cpp.obj"
  "C:/yaml-cpp/test/node/node_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/node/node_test.cpp.obj"
  "C:/yaml-cpp/test/ostream_wrapper_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/ostream_wrapper_test.cpp.obj"
  "C:/yaml-cpp/test/parser_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/parser_test.cpp.obj"
  "C:/yaml-cpp/test/regex_test.cpp" "C:/yaml-cpp/cmake-build-debug/test/CMakeFiles/yaml-cpp-tests.dir/regex_test.cpp.obj"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "../test/integration"
  "../test"
  "../src"
  "../include"
  "../test/gtest-1.8.0/googlemock/include"
  "../test/gtest-1.8.0/googletest/include"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  "C:/yaml-cpp/cmake-build-debug/CMakeFiles/yaml-cpp.dir/DependInfo.cmake"
  "C:/yaml-cpp/cmake-build-debug/test/prefix/googlemock/CMakeFiles/gmock.dir/DependInfo.cmake"
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
