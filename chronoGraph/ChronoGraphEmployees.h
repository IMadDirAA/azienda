//
// Created by imadd on 31/03/2020.
//

#ifndef COMPANY_CHRONOGRAPHEMPLOYEES_H
#define COMPANY_CHRONOGRAPHEMPLOYEES_H


#include <vector>
#include <algorithm>
#include "ChronoGraph.h"
#include "../employee/EmployeesComponent.h"
#include "../passage/PassagesComponent.h"

using namespace std;

class ChronoGraphEmployees {
private:
    static EmployeesComponent* employeesComponent;
    static PassagesComponent* passagesComponent;
public:
    /**
     * default constructor which initialize propriety
     */
    ChronoGraphEmployees();

    /**
     * function getChronoGraphs()
     * @param employee
     * @return a vector of chronoGraph
     */
    vector<ChronoGraph> getChronoGraphs(Employee employee);
};


#endif //COMPANY_CHRONOGRAPHEMPLOYEES_H
