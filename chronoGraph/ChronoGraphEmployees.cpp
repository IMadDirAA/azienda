//
// Created by imadd on 31/03/2020.
//

#include "ChronoGraphEmployees.h"

vector<ChronoGraph> ChronoGraphEmployees::getChronoGraphs(Employee employee) {
    // empty chronoGraph vector
    vector<ChronoGraph> chronoGraphs;

    // get all employee passages
    vector<Passage> employeePassages = ChronoGraphEmployees::passagesComponent->read([&employee](Passage passage){
        return passage.getId() == employee.getID();
    });

    // if there is no passages return a empty chronoGraphs
    if (employeePassages.empty())
        return chronoGraphs;

    // sort passages by time
    sort(employeePassages.begin(), employeePassages.end(), [](Passage a, Passage b){
       return a.getTime() < b.getTime();
    });

    // process data
    // variables used for process
    ChronoGraph *chronoGraph;
    Passage *ingressPassage = nullptr;
    Passage *egressPassage = nullptr;
    Passage *passage;
    vector<Passage>::iterator iteratorPassage = employeePassages.begin();
    while (iteratorPassage != employeePassages.end()) {
        if (iteratorPassage->getPassageType() == PassageType::ingress) {
            ingressPassage = new Passage(iteratorPassage->getTime(), iteratorPassage->getPassageType(), iteratorPassage->getId());
            iteratorPassage++;
        }

        if (iteratorPassage != employeePassages.end() && iteratorPassage->getPassageType() == PassageType::egress) {
            egressPassage = new Passage(iteratorPassage->getTime(), iteratorPassage->getPassageType(), iteratorPassage->getId());
            iteratorPassage++;
        }

        chronoGraph = new ChronoGraph(&employee, ingressPassage, egressPassage);
        chronoGraphs.push_back(*chronoGraph);
        ingressPassage = nullptr;
        egressPassage = nullptr;
    }
    return chronoGraphs;
}

ChronoGraphEmployees::ChronoGraphEmployees() {
    if (!ChronoGraphEmployees::employeesComponent and !ChronoGraphEmployees::passagesComponent) {
        ChronoGraphEmployees::employeesComponent = new EmployeesComponent();
        ChronoGraphEmployees::passagesComponent = new PassagesComponent();
    }
}

EmployeesComponent *ChronoGraphEmployees::employeesComponent = nullptr;
PassagesComponent *ChronoGraphEmployees::passagesComponent = nullptr;