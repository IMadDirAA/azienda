//
// Created by imadd on 31/03/2020.
//

#include <ctime>
#include "ChronoGraph.h"

ChronoGraph::ChronoGraph(Employee *employee, Passage *ingressPassage, Passage *egressPassage) : employee(employee),
                                                                                                ingressPassage(
                                                                                                        ingressPassage),
                                                                                                egressPassage(
                                                                                                        egressPassage) {}

tm *ChronoGraph::getIngressLtm() {
    if (this->ingressPassage)
        return this->ingressPassage->getLtm();
    return nullptr;
}

tm *ChronoGraph::getEgressLtm() {
    if (this->egressPassage)
        return this->egressPassage->getLtm();
    return nullptr;
}

string ChronoGraph::getIngressDate() {
    if (this->ingressPassage) {
        time_t temp = this->ingressPassage->getTime();
        return string(ctime(&temp));
    }
    return "";
}

string ChronoGraph::getEgressDate() {
    if (this->egressPassage) {
        time_t temp = this->egressPassage->getTime();
        return string(ctime(&temp));
    }
    return "";
}

const Employee &ChronoGraph::getEmployee() {
    return *(this->employee);
}

const Passage *ChronoGraph::getIngressPassage() {
    return this->ingressPassage;
}

const Passage *ChronoGraph::getEgressPassage() {
    return this->egressPassage;
}

double ChronoGraph::getDiffTime() {
    if (this->ingressPassage and this->egressPassage)
        return difftime(this->egressPassage->getTime(), this->ingressPassage->getTime());
    return 0;
}
