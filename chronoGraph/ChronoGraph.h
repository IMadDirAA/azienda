//
// Created by imadd on 31/03/2020.
//

#ifndef COMPANY_CHRONOGRAPH_H
#define COMPANY_CHRONOGRAPH_H

#include <string>
#include "../employee/Employee.h"
#include "../passage/Passage.h"
using namespace std;

class ChronoGraph {
public:
    ChronoGraph(Employee *employee, Passage *ingressPassage, Passage *egressPassage);
    tm *getIngressLtm();
    tm *getEgressLtm();
    string getIngressDate();
    string getEgressDate();
    const Employee &getEmployee();
    const Passage *getIngressPassage();
    const Passage *getEgressPassage();
    double getDiffTime();
private:
    Employee *employee;
    Passage *ingressPassage;
    Passage *egressPassage;
};


#endif //COMPANY_CHRONOGRAPH_H
