This is an intro sentence to my Wiki page.

[[_TOC_]]


## Introduction

:office: company app :school: school project that can be used to **manage employees passages**

the core app is construct by three fundamentals components:
1. **employees** :man:
2. **passages** :construction:
3. **scheduler** :date:

## employees component:
the employees component is the part that deals with adding and removing and searching for employees 

### Usage:
        employees {show | remove} {[--all] | [--id <id>] [--first_name <first_name>] [--last_name <last_name>] [--work <work>] [--age <age>] [--limit <limit>]}
        employees add

options:
- --id specify the employee id and is optionally
- --first_name specify the employee firstname and is optionally
- --lastname specify the employee lastname and is optionally
- --age specify the employee age and is optionally
- --work specify the employee work and is optionally
- --all specify all employees and is optionally

**note**: all fields are optionally but at least one must be specified or you can use --all option that include all

### adding employee example:

        company employees add

after running the command the program will ask you for the basic employee data as follow:
    <div align="center">
    ![alt text](media/employee%20adding%20example.PNG "adding employee example")
    </div>

### show employee example:

#### show all employees

        company employees show --all
        
after running the command the program will show you all employees as follow:
    <div align="center">
    ![alt text](media/employee%20show%20all%20example.PNG "adding employee example")
    </div>

#### show employees by fields

        company employees show --id <id>
        
after running the command the program will show you employees with the given fields as follow:
    <div align="center">
    ![alt text](media/employee%20show%20by%20fields%20example.PNG "adding employee example")
    </div>

### remove employee example:

#### remove by fields
       <div style="background-color:black">
       company employees remove --id <id> --first_name <firstname> --last_name <lastname> --work <work> --age <age> --limit <div>
       </div>

after running the command the program will remove all employees with the given fields as follow:
    <div align="center">
    ![alt text](media/employee%20remove%20by%20fields%20example.PNG "adding employee example")
    </div>
    
#### remove all

       company employees remove --all

after running the command the program will remove all employees as follow:
    <div align="center">
    ![alt text](media/employee%20remove%20by%20fields%20example.PNG "adding employee example")
    </div>
    

## passages component:
the passages component is the part that deals with adding and searching for passages

### Usage:
    passages show {--all | [--passage_type <passage_type>]} [--limit <limit>]
    passages add <id> <passage_type>

### adding example:

        company employees add
        

### show example:

        company show --all
        

## employees component:
the employees component is the part that deals with adding and removing and searching for employees 

### Usage:
        employees {show | remove} {[--all] | [--id <id>] [--first_name <first_name>] [--last_name <last_name>] [--work <work>] [--age <age>] [--limit <limit>]}
        employees add

### adding example:

        company employees add
        

### remove example:

        company remove --id sk3wlw --first_name name
        

### show example:

        company show --all
        


   